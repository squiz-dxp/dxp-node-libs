/* global describe, test, expect */
const {send} = require('../index');

// Helper Fakes.
// ---------------------------------------------------------------------------------

// Create a fake auth function.
const fakeAuth = () => Promise.resolve({
  username: 'Test',
});

// Create a fake fetch function.
const fakeFetch = (url, params) => Promise.resolve({
  json:    () => ((params.headers.Accept === 'application/json') ? Promise.resolve('200 OK') : null),
  text:    () => ((params.headers.Accept !== 'application/json') ? Promise.resolve('200 OK') : null),
  headers: new Map([
    ['content-type', (params.headers.Accept === 'application/json') ? 'application/json' : 'text/text'],
  ]),
});


// Actual tests.
// ---------------------------------------------------------------------------------

describe('send()', () => {
  test(
    'should complete basic GET request',
    () => send(fakeFetch, 'get', 'http://httpstat.us', fakeAuth, '/200', {}, {timeout: 0})
      .then((resp) => {
        expect(resp).toEqual('200 OK');
      }),
  );

  test(
    'should decode JSON data',
    () => send(
      fakeFetch,
      'get',
      'http://httpstat.us',
      fakeAuth,
      '/200',
      {
        headers: {
          Accept: 'application/json',
        },
      }, {
        timeout: 0,
      },
    )
      .then((resp) => {
        expect(resp).toEqual('200 OK');
      }),
  );
});
