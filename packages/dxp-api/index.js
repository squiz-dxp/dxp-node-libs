const isoFetch = require('isomorphic-unfetch');
const { clone, curry, paramsToQueryString } = require('@squiz-dxp/util');

/**
 * Send a request to an API endpoint.
 *
 * @param {function} fetch   The global fetch() function. Used for testing.
 * @param {string}   method  The fetch method.
 * @param {string}   host    The host for the API endpoint (including protocol).
 * @param {function} authFn  Async auth function (TODO handle return data).
 * @param {string}   path    The path for the api endpoint.
 * @param {object}   params  Params to include with the request.
 * @param {object}   options Any options specific to the usage of this function.
 */
function send(
  fetch,
  method,
  host,
  authFn,
  path,
  params = {},
  options = {},
) {
  const _method = method.toLowerCase();

  const _opts = Object.assign({
    format:  'json',
    timeout: 30,
    headers: {},
  }, options);

  const _params = clone(params);
  let data    = _params;
  let apiURL  = [host.replace(/\/$/, ''), path.replace(/^\//, '')].join('/');

  if (_method === 'get') {
    data = paramsToQueryString(_params);
    _opts.headers.Pragma = 'no-cache';
    _opts.headers['Cache-Control'] = 'no-cache';
  } else {
    _opts.headers['Content-Type'] = 'application/json';
  }

  const reqParams = {
    method:      _method,
    headers:     _opts.headers,
    redirect:    'follow',
    credentials: 'include',
  };

  if (['post', 'put', 'patch'].indexOf(_method) !== -1) {
    reqParams.body = JSON.stringify(data);
  } else if (typeof data === 'string' && data !== '') {
    apiURL = `${apiURL}?${data}`;
  }

  const pTimeout = new Promise((resolve, reject) => {
    setTimeout(reject, _opts.timeout * 1000, 'request timed out');
  });

  const sendRequest = () => {
    const pRequest = fetch(apiURL, reqParams)
      .then((response) => {
        if (response.status === 204) {
          return null;
        }

        const contentType = response.headers.get('content-type');
        let respData = null;

        if (contentType && contentType.indexOf('application/json') !== -1) {
          respData = response.json();
        } else {
          respData = response.text();
        }

        if (response.ok === false) {
          throw new Error(response.status);
        }

        return respData;
      });

    const p = Promise.race([pTimeout, pRequest]);
    return p;
  };

  return authFn().then(sendRequest);
}

// Host is sourced from API_HOST environment variable, OR
// appConfig.api.host OR
// falls back to the default given below.
const getAPI = (name, hostDomain, authProvider) => {
  const host = `${hostDomain.replace(/\/$/, '')}/api/${name}/`;

  return {
    get:   curry(send, isoFetch, 'get', host, authProvider),
    post:  curry(send, isoFetch, 'post', host, authProvider),
    patch: curry(send, isoFetch, 'patch', host, authProvider),
    put:   curry(send, isoFetch, 'put', host, authProvider),
    del:   curry(send, isoFetch, 'delete', host, authProvider),
    send,
    host,
  };
};

module.exports = getAPI;
module.exports.send = send;
