/* eslint-disable prefer-arrow-callback */
const express               = require('express');
const next                  = require('next');
const nextI18NextMiddleware = require('next-i18next/middleware');
const fetch                 = require('isomorphic-unfetch');
const locale                = require('@squiz-dxp/locale');
const https                 = require('https');
const cors                  = require('cors');

const {handleServerError, proxyAPIRequest} = require('./serverlib');

const {env} = process;

// TODO: @abarnes
// - Secure server certificates
// - require('http2)

module.exports = (appConfig, dev, port, defaultHost) => {
  const app = next({ dev });

  return app.prepare()
    .then(() => {
      const server      = express();
      const handle      = app.getRequestHandler();
      const nextI18next = locale(appConfig.defaultLanguage, appConfig.otherLanguages);

      server.use(cors());
      server.use(nextI18NextMiddleware(nextI18next));

      server.get('/api/*', proxyAPIRequest({fetch, env, appConfig, defaultHost}));
      server.get('*', handle);

      server.listen(port, function listen(err) {
        if (err) return Promise.reject(err);
        console.log(`> Ready on http://localhost:${port}`); // eslint-disable-line
        return null;
      });
    })
    .catch(handleServerError(console, process));
};

module.exports.startSecureServer = (appConfig, dev, port, defaultHost) => {
  const app = next({ dev });

  if (!appConfig.tls) {
    throw new Error('TLS config is required {tls: {key: ..., cert: ...}}');
  }

  return app.prepare()
    .then(() => {
      const server      = express();
      const handle      = app.getRequestHandler();
      const nextI18next = locale(appConfig.defaultLanguage, appConfig.otherLanguages);

      server.use(cors());
      server.use(nextI18NextMiddleware(nextI18next));

      server.get('/api/*', proxyAPIRequest({fetch, env, appConfig, defaultHost}));
      server.get('*', handle);

      // server.listen(port, function listen(err) {
      //   if (err) return Promise.reject(err);
      //   console.log(`> Ready on http://localhost:${port}`); // eslint-disable-line
      //   return null;
      // });

      https
        .createServer(appConfig.tls, server)
        .listen(port, function listen(err) {
          if (err) return Promise.reject(err);
          console.log(`> Ready on https://localhost:${port}`); // eslint-disable-line
          return null;
        });
    })
    .catch(handleServerError(console, process));
};
