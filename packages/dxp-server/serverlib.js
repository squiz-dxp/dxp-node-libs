/**
 * Utility functions for Server.
 *
 * Note that these are not transpiled by babel, so we have to use require()
 * rather than import.
 */
/* eslint-disable prefer-arrow-callback */

const { curry } = require('@squiz-dxp/util');

/**
 * Get Base URL for the API.
 *
 * @param {object} env The environment object from process.env
 * @param {object} conf The configuration object for the app.
 * @param {string} deflt A default fallback value if host is unspecified.
 */
function getHost(env, conf, deflt) {
  return (env && env.API_HOST)
    || (conf && conf.api && conf.api.host)
    || deflt;
}

/**
 * Handle Server Error.
 *
 * @param {object} consl The global console object.
 * @param {object} proc  The global process object.
 * @param {error}  err   The error caught by the server..
 */
const handleServerError = curry(function handleServerError(consl, proc, err) {
  consl.error(err);
  proc.exit(1);
});

/**
 * Proxy API Request to API.
 * @param {object} dependencies Things needed to make this function work.
 * @param {object} req          The request object.
 * @param {object} res          The response object.
 */
const proxyAPIRequest = curry(function proxyAPIRequest({fetch, env, appConfig, defaultHost}, req, res) {
  const base     = getHost(env, appConfig, defaultHost).replace(/\/$/, '');
  const basePath = `/api/${appConfig.name}`;
  const path     = req.url.replace(/^\/api/, '');
  const url      = `${base}${basePath}${path}`;
  return fetch(url, {headers: {Accept: 'application/json'}})
    .then(x => x.json())
    .then((json) => {
      res.setHeader('Content-Type', 'application/json');
      return res.end(JSON.stringify(json));
    });
});

// We use module.exports instead of export default here.
module.exports = {
  getHost,
  handleServerError,
  proxyAPIRequest,
};
