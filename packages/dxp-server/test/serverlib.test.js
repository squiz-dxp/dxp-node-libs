/**
 * Tests for Server Library.
 */
/* global describe, test, expect */
const {
  getHost,
  handleServerError,
  proxyAPIRequest,
} = require('../serverlib');


// Helper Fakes.
// ---------------------------------------------------------------------------------

// Create a fake fetch function.
const fakeFetch = (url, params) => Promise.resolve({
  json:    () => ((params.headers.Accept === 'application/json') ? Promise.resolve('200 OK') : null),
  text:    () => ((params.headers.Accept !== 'application/json') ? Promise.resolve('200 OK') : null),
  headers: new Map([
    ['content-type', (params.headers.Accept === 'application/json') ? 'application/json' : 'text/text'],
  ]),
});


// Actual tests.
// ---------------------------------------------------------------------------------

describe('getHost()', () => {
  test('should decide host according to business rules', () => {
    const tests = [
      {env: null, appConfig: null, expected: 'http://localhost:8000/api/squiz/heportal/'},
      {env: null, appConfig: {api: {host: 'http://example.com'}}, expected: 'http://example.com'},
      {
        env:       {API_HOST: 'http://example.com/path'},
        appConfig: {api: {host: 'http://example.com'}},
        expected:  'http://example.com/path',
      },
    ];
    tests.forEach(({env, appConfig, expected}) => {
      const actual = getHost(env, appConfig, 'http://localhost:8000/api/squiz/heportal/');
      expect(actual).toEqual(expected);
    });
  });
});

describe('handleServerError()', () => {
  test('should log an error to the console', () => {
    expect.assertions(1);
    const fakeErr = new Error('Fake error');
    const fakeConsole = {
      error: err => expect(err).toBe(fakeErr),
    };
    const fakeProcess = {
      exit: () => {},
    };
    handleServerError(fakeConsole, fakeProcess)(fakeErr);
  });
  test('should call process.exit()', () => {
    const fakeErr = new Error('Fake error');
    expect.assertions(1);
    const fakeConsole = {
      error: () => {},
    };
    const fakeProcess = {
      exit: retVal => expect(retVal).not.toEqual(0),
    };
    handleServerError(fakeConsole, fakeProcess)(fakeErr);
  });
});

describe('proxyAPIRequest()', () => {
  test('should make a HTTP call to the api, given a fetch function and baseURL', () => {
    expect.assertions(4);
    const expected    = '200 OK';
    const reqPath     = '/api/foo/bar';
    const req         = {url: reqPath};
    const defaultHost = 'http://example.com/';
    const ff = (url, params) => fakeFetch(url, params).then((x) => {
      expect(url).toEqual('http://example.com/api/squiz/heportal/foo/bar');
      return x;
    });
    const dependencies = {
      fetch:     ff,
      env:       {},
      appConfig: {name: 'squiz/heportal'},
      defaultHost,
    };
    const res = {
      end: (data) => {
        expect(data).toEqual(JSON.stringify(expected));
        return data;
      },
      setHeader: (key, val) => expect([key, val]).toEqual(['Content-Type', 'application/json']),
    };

    return proxyAPIRequest(dependencies)(req, res).then((resp) => {
      expect(resp).toEqual(JSON.stringify(expected));
    });
  });
});
