const auth0 = require('auth0-js');

// TODO: @abarnes convert to class for auth0 var storage?

function getAuth0(clientID, domain) {
  return new auth0.WebAuth({
    clientID,
    domain,
  });
}

// TODO: won't work for server side auth or testing.
const getBaseUrl = () => `${window.location.protocol}//${window.location.host}`;

const getOptions = () => ({
  responseType: 'token id_token',
  redirectUri:  `${getBaseUrl()}/auth/signed-in`,
  scope:        'openid profile email',
});

module.exports.authorize = (clientID, domain) => getAuth0(clientID, domain).authorize(getOptions());
module.exports.logout = (clientID, domain) => getAuth0(clientID, domain).logout({ returnTo: getBaseUrl() });
module.exports.parseHash = (clientID, domain, callback) => getAuth0(clientID, domain).parseHash(callback);
