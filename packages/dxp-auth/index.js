
const jwtDecode = require('jwt-decode');
const Cookie = require('js-cookie');

const setToken = (idToken, accessToken) => {
  if (!process.browser) {
    return;
  }
  Cookie.set('user', jwtDecode(idToken));
  Cookie.set('idToken', idToken);
  Cookie.set('accessToken', accessToken);
};

const unsetToken = () => {
  if (!process.browser) {
    return;
  }
  Cookie.remove('idToken');
  Cookie.remove('accessToken');
  Cookie.remove('user');

  // to support logging out from all windows
  window.localStorage.setItem('logout', Date.now());
};

const getJWTFromServerCookie = (req) => {
  if (!req.headers.cookie) {
    return undefined;
  }
  const jwtCookie = req.headers.cookie.split(';').find(c => c.trim().startsWith('idToken='));
  if (!jwtCookie) {
    return undefined;
  }
  const jwt = jwtCookie.split('=')[1];
  return jwt;
};

const getUserFromServerCookie = (req) => {
  const jwt = getJWTFromServerCookie(req);
  if (!jwt) {
    return undefined;
  }

  return jwtDecode(jwt);
};

const getJWTFromLocalCookie = () => Cookie.get('idToken');

const getUserFromLocalCookie = () => Cookie.getJSON('user');

const authProvider = async () => ({
  username: 'Test',
});

module.exports = authProvider;
module.exports.setToken = setToken;
module.exports.unsetToken = unsetToken;
module.exports.getJWTFromServerCookie = getJWTFromServerCookie;
module.exports.getUserFromServerCookie = getUserFromServerCookie;
module.exports.getJWTFromLocalCookie = getJWTFromLocalCookie;
module.exports.getUserFromLocalCookie = getUserFromLocalCookie;
