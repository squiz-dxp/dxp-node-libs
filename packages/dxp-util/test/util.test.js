/* global describe, test, expect */
const {clone} = require('../index');

describe('clone', () => {
  test('clone should return an identical copy of an object', () => {
    const input = {
      foo: 'bar',
      baz: 'qux',
    };
    const actual = clone(input);
    expect(actual).not.toBe(input);
    expect(actual).toEqual(input);
  });
});
