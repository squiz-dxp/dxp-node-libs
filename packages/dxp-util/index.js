/**
 * Return a curried function with supplied arguments.
 *
 * @param Function fn    The function to curry.
 * @param ...mixed args1 Arguments to supply.
 *
 * @return function
 */
const curry = (f, ...args) => {
  if (f.length <= args.length) {
    return f(...args);
  }

  return (...more) => curry(f, ...args, ...more);
};

/**
 * Return a new state object given previous state and new values.
 *
 * @param object prevState Previous state object.
 * @param object newState  New state values to assign.
 *
 * Adapted from this: https://jsperf.com/structured-clone-objects/2
 *
 * @return object
 */
const clone = (obj) => {
  let copy;

  // Values that cannot be copied.
  if (obj === null || typeof obj !== 'object') {
    return obj;
  }

  // Handle Date.
  if (obj instanceof Date) {
    copy = new Date();
    copy.setTime(obj.getTime());
    return copy;
  }

  // Handle Array.
  if (Array.isArray(obj) === true) {
    copy = [];
    for (let i = 0, len = obj.length; i < len; i += 1) {
      copy[i] = clone(obj[i]);
    }
    return copy;
  }

  // Handle Object.
  if (obj instanceof Object) {
    copy = {};

    Object.keys(obj).forEach((attr) => {
      copy[attr] = clone(obj[attr]);
    });

    return copy;
  }

  throw new Error('Unable to copy obj! Its type isn\'t supported.');
};

/**
 * Convert param object to query string.
 *
 * @param {object} p Object with key value pairs.
 */
const paramsToQueryString = p => Object.keys(p).map((k) => {
  let v = p[k];
  if (typeof v !== 'string') {
    v = JSON.stringify(v);
  }

  return `${encodeURIComponent(k)}=${encodeURIComponent(v)}`;
}).join('&');

module.exports = {
  curry,
  clone,
  paramsToQueryString,
};
