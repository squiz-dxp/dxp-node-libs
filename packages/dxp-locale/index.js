const NextI18Next = require('next-i18next').default;

module.exports = (defaultLanguage, otherLanguages) => new NextI18Next({
  localeSubpaths: 'foreign',
  defaultLanguage,
  otherLanguages,
});
